<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = auth()->user()->products;
        $user = Auth::user();

        $categories = Category::with('parent')->where('user_id', $user->id)->get();

        $categoriesList = [];

        foreach ($categories as $category) {
            $categoryItem = [
                'id' => $category->id,
                'name' => $category->name ,
                'parent_id' => $category->parent_id,
                'parent_name' => $category->parent ? $category->parent->name : '',
            ];

            $categoriesList[] = $categoryItem;
        }
        $query = null;
        $category_id = null;
        return view('products.index', compact('products', 'categoriesList', 'query', 'category_id'));
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $user = Auth::user();

        $categories = Category::with('parent')->where('user_id', $user->id)->get();

        $categoriesList = [];

        foreach ($categories as $category) {
            $categoryItem = [
                'id' => $category->id,
                'name' => $category->name ,
                'parent_id' => $category->parent_id,
                'parent_name' => $category->parent ? $category->parent->name : '',
            ];

            $categoriesList[] = $categoryItem;
        }
        return view('products.create', compact('categoriesList'));
    }

    public function search(Request $request)
    {
        $query = $request->input('query');
        $category_id = $request->input('category');

        $products = Product::where('user_id', auth()->user()->id)
            ->when($query, function ($query, $search) {
                return $query->where(function ($query) use ($search) {
                    $query->where('user_id', auth()->user()->id)
                        ->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('description', 'LIKE', "%{$search}%")
                        ->orWhere('price', 'LIKE', "%{$search}%")
                        ->orWhere('quantity', 'LIKE', "%{$search}%");
                });
            })
            ->when($category_id, function ($query, $category_id) {
                return $query->where('category_id', $category_id);
            })
            ->get();



            $user = Auth::user();

            $categories = Category::with('parent')->where('user_id', $user->id)->get();

            $categoriesList = [];

            foreach ($categories as $category) {
                $categoryItem = [
                    'id' => $category->id,
                    'name' => $category->name ,
                    'parent_id' => $category->parent_id,
                    'parent_name' => $category->parent ? $category->parent->name : '',
                ];

                $categoriesList[] = $categoryItem;
            }

        return view('products.index', compact('products', 'categoriesList', 'query', 'category_id'));
    }



    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'category' => 'required|exists:categories,id',
        ]);

        $product = new Product();
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->quantity = $request->input('quantity');
        $product->user_id = auth()->user()->id;
        $product->category_id = $request->input('category');
        $product->save();

        return redirect()->route('products.index')->with('success', 'Product created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::find($id);

        if (!$product) {
            return response()->json(['success' => false, 'message' => 'Product not found'], 404);
        }


        $categoryName = $product->category->name;

        return response()->json(['success' => true, 'product' => $product, 'category_name' => $categoryName]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::find($id);
        $user = Auth::user();

        $categories = Category::with('parent')->where('user_id', $user->id)->get();

        $categoriesList = [];

        foreach ($categories as $category) {
            $categoryItem = [
                'id' => $category->id,
                'name' => $category->name ,
                'parent_id' => $category->parent_id,
                'parent_name' => $category->parent ? $category->parent->name : '',
            ];

            $categoriesList[] = $categoryItem;
        }
        return view('products.edit', compact('product', 'categoriesList'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->quantity = $request->input('quantity');
        $product->save();


        return redirect()->route('products.index')->with('success', 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect()->route('products.index');
    }
}
