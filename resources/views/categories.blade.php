@extends('layouts.app')

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show container" id="success-message">
    <strong>{{ $message }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show container" id="error-message">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Category') }}</div>
                    <div class="card-body">

                        <form method="POST" action="{{ route('categories.store') }}" class="container" id="categoryForm">
                            @csrf
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Category Name') }}</label>
                                <div class="col-md-6">
                                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control" required>
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="parent_id" class="col-md-4 col-form-label text-md-end">{{ __('Parent Category') }}</label>
                                <div class="col-md-6">
                                    <select name="parent_id" id="parent_id" class="form-control" >
                                        <option value="">Select Category</option>

                                        @foreach ($categoriesList as $category)
                                            <option value="{{ $category['id'] }}" {{ $category['id'] == old('parent_id') ? 'selected' : '' }}>{{ $category['name'] }} @if($category['parent_id']) ({{ $category['parent_name'] }}) @endif</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">{{ __('Create Category') }}</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
