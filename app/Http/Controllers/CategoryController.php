<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();

        $categories = Category::with('parent')->where('user_id', $user->id)->get();

        $categoriesList = [];

        foreach ($categories as $category) {
            $categoryItem = [
                'id' => $category->id,
                'name' => $category->name ,
                'parent_id' => $category->parent_id,
                'parent_name' => $category->parent ? $category->parent->name : '',
            ];

            $categoriesList[] = $categoryItem;
        }

        return view('categories', compact('categoriesList'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:categories',
            'parent_id' => 'nullable|exists:categories,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $category = new Category;
        $category->name =  $request->name;
        $category->parent_id = $request->parent_id;
        $category->user_id = auth()->user()->id;
        $category->save();

        return redirect()->back()->with('success', 'Category added successfully!');


    }
}
