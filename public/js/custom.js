$(document).ready(function() {
    //product Form validation
    $("#productForm").validate({
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function(element, errorClass, validClass) {
            $(element).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass("is-invalid").addClass("is-valid");
        },
        errorPlacement: function(error, element) {
            error.appendTo(element.closest(".col-md-6"));
        },
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            },
            price: {
                required: true
            },
            quantity: {
                required: true
            },
            category: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Product Name is required."
            },
            description: {
                required: "Description is required."
            },
            price: {
                required: "Price is required."
            },
            quantity: {
                required: "Quantity is required."
            },
            category: {
                required: "Category is required."
            }
        }
    });

    //category Form Validation
    $("#categoryForm").validate({
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function(element, errorClass, validClass) {
            $(element).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass("is-invalid").addClass("is-valid");
        },
        errorPlacement: function(error, element) {
            error.appendTo(element.closest(".col-md-6"));
        },
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Category Name is required."
            }
        }
    });

    // delete button
    document.querySelectorAll('.delete-button').forEach(button => {
        button.addEventListener('click', function(event) {
            event.preventDefault();

            // Show confirmation dialog
            Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this product!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'Cancel'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Submit the form if confirmed
                    event.target.closest('.delete-form').submit();
                }
            });
        });
    });

    //View Product
    $('.view-product').on('click', function(e) {
        e.preventDefault();

        var productId = $(this).data('id');

        $.ajax({
            url: '/products/' + productId,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                // Handle the AJAX response and display the product details in a Swal popup
                if (response.success) {
                    var product = response.product;
                    var category_name = response.category_name;
                    // Display the product details in the Swal popup
                    Swal.fire({
                        title: product.name,
                        html: '<strong>Category:</strong> ' + category_name + '<br>' +
                            '<strong>Description:</strong> ' + product.description + '<br>' +
                            '<strong>Price:</strong> ' + product.price + '<br>' +
                            '<strong>Quantity:</strong> ' + product.quantity,
                        confirmButtonText: 'Close'
                    });
                }
            },
            error: function(xhr, status, error) {
                // Handle the error response
                console.log(error);
            }
        });
    });

    //  remove the success message
    setTimeout(function() {
        var successMessage = document.getElementById('success-message');
        if (successMessage) {
            successMessage.classList.remove('show');
            setTimeout(function() {
                successMessage.remove();
            }, 200);
        }
    }, 3000);

    //  remove the error message
    setTimeout(function() {
        var errorMessage = document.getElementById('error-message');
        if (errorMessage) {
            errorMessage.classList.remove('show');
            setTimeout(function() {
                errorMessage.remove();
            }, 200);
        }
    }, 4000);

});
