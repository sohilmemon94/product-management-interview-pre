@extends('layouts.app')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show container" id="success-message">
            <strong>{{ $message }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show container">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <h1 class="card-header d-flex justify-content-between align-items-center">{{ __('All Products') }}<a
                            href="{{ url('products/create') }}" class="btn btn-primary">Add Product</a></h1>
                    <div class="card-body">
                        <form action="{{ route('products.search') }}" method="POST" class="mb-4">
                            @csrf
                            <div class="form-row">
                                <div class="row mb-3">
                                    <div class="col-md-5">
                                        <input type="text" name="query" class="form-control"
                                            placeholder="Search by name" value="{{ $query }}">
                                    </div>
                                    <div class="col-md-5">
                                        <select name="category" class="form-control">
                                            <option value="">All Categories</option>
                                            @foreach ($categoriesList as $category)
                                                <option value="{{ $category['id'] }}"
                                                    {{ $category['id'] == $category_id ? 'selected' : '' }}>
                                                    {{ $category['name'] }}  @if($category['parent_id']) ({{ $category['parent_name'] }}) @endif</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2"><button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                        <hr>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->description }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->quantity }}</td>
                                        <td>
                                            <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary" >Edit</a>
                                            <a href="#" class="btn btn-primary view-product" data-id="{{ $product->id }}">View</a>

                                            <form action="{{ route('products.destroy', $product->id) }}" method="POST"
                                                class="delete-form" style="display: inline;">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-primary delete-button">Delete</button>

                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
