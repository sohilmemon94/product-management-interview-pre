@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit Product') }}</div>
                    <div class="card-body">
                        <form action="{{ route('products.update', $product->id) }}" method="POST" class="container" id="productForm">
                            @csrf
                            @method('PUT')
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Name:</label>
                                <div class="col-md-6">
                                    <input type="text" name="name" class="form-control" value="{{ $product->name }}"
                                        required>
                                        @error('name')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="description" class="col-md-4 col-form-label text-md-end">Description:</label>
                                <div class="col-md-6">
                                    <textarea name="description" class="form-control" required>{{ $product->description }}</textarea>
                                    @error('description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="price" class="col-md-4 col-form-label text-md-end">Price:</label>
                                <div class="col-md-6">
                                    <input type="number" min="0"  step="0.01" name="price" class="form-control"
                                        value="{{ $product->price }}" required>
                                    @error('price')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="quantity" class="col-md-4 col-form-label text-md-end">Quantity:</label>
                                <div class="col-md-6">
                                    <input type="number" min="0" name="quantity" class="form-control"
                                        value="{{ $product->quantity }}" required>
                                    @error('quantity')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="category" class="col-md-4 col-form-label text-md-end">Category</label>
                                <div class="col-md-6">
                                    <select name="category" class="form-control" required>
                                        <option value="">Select Category</option>
                                        @foreach ($categoriesList as $category)
                                            <option value="{{ $category['id'] }}"
                                                {{ $category['id'] === $product->category->id ? 'selected' : '' }}>
                                                {{ $category['name'] }} @if($category['parent_id']) ({{ $category['parent_name'] }}) @endif
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary ">{{ __('Update Product') }}</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
